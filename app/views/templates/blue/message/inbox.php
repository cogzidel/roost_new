<link href="<?php echo css_url().'/dashboard.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<div class="container-fluid">
<?php $this->load->view(THEME_FOLDER.'/includes/dash_header'); ?>
<div class="row-fluid">
	<div class="span12">
		<div id="dashboard_container">
<div class="" id="Msg_Inbox_Big">
	<div class="row-fluid">
		<div class="span2">
    <div class="Box_Head msgbg gotomsg">
      <h2><?php echo translate("Inbox"); ?></h2>
     </div>
    </div>
   </div>
   
      <div class="Box_Content">
      	<div class="row-fluid">
   	<div class="span12">
			<div id="message" style="padding:0 0 10px 0;"></div>
            <ul>
            	<div class="row-fluid">
            		<div class="span12">		
            							<?php
																 if($messages->num_rows() > 0) 
																 {
																		foreach($messages->result() as $row) { //print_r($row); 
			 if($row->contact_id != 0)
			 {
			 $checkin=$this->Common_model->getTableData('contacts',array('id' => $row->contact_id))->row()->checkin; 
             $checkout=$this->Common_model->getTableData('contacts',array('id' => $row->contact_id))->row()->checkout; 
			 }
			 else 
			 {			
			 $checkin=$this->Common_model->getTableData('reservation',array('id' => $row->reservation_id))->row()->checkin;
			 $checkin=date('m/d/y',$checkin);  
             $checkout=$this->Common_model->getTableData('reservation',array('id' => $row->reservation_id))->row()->checkout;
             $checkout=date('m/d/y',$checkout);  
             }																
																			?>	
																		
						
													 	<li class="clearfix msg_style" <?php if($row->is_read == 0) echo 'style="color:#00B0FF"'; else echo 'style="background:#FFFFD0; background-color: white; color:#00B0FF"';  ?> >
                    		<div class="span1">		
                    	<div class="clsMsg_User clsFloatLeft">
                        	<a href="<?php echo site_url('users/profile').'/'.$row->userby; ?>"><img  width="100%" alt="" src="<?php echo $this->Gallery->profilepic($row->userby,2); ?>" /></a>
                            
                            <!--31 minutes-->
                        </div>
                       </div>
                       <div class="span8">
                        <div class="clsMeg_Detail clsFloatLeft">
                       <p><a href="<?php echo site_url('users/profile').'/'.$row->userby; ?>"><?php echo get_user_by_id($row->userby)->username; ?></a> </p>
                            
																										<?php
																											if($row->conversation_id != 0) $message_id = $row->conversation_id; else $message_id = $row->reservation_id;
																											if($message_id == 0) $message_id = $row->contact_id;
																											if($row->message_type == 6)	{ 
									
																										$subject = 'Inquiry about '.substr(get_list_by_id($row->list_id)->title,0,10);
																										if($row->is_read == 0) echo '<strong>'; echo anchor(''.$row->url.'/'.$row->conversation_id, $subject, array("onclick" => "javascript:is_read(".$row->id.")")); if($row->is_read == 0) echo '</strong>'; 
																														
																											} 
																										else if($row->message_type == 3 || $row->message_type == 2)	{ 
									
																										$subject = 'Discuss about '.substr(get_list_by_id($row->list_id)->title,0,10);
																										if($row->is_read == 0) echo '<strong>'; echo anchor(''.$row->url.'/'.$row->conversation_id, $subject, array("onclick" => "javascript:is_read(".$row->id.")")); if($row->is_read == 0) echo '</strong>'; 
																														
																											}
																											
																											
																											
																											else { 
																											
																								if($row->is_read == 0) echo '<strong>'; echo anchor(''.$row->url.'/'.$message_id, $row->message, array("onclick" => "javascript:is_read(".$row->id.")")); if($row->is_read == 0) echo '</strong>'; ?>
																										
																										
																										<p class="msg_big_spn"><span><?php echo substr(get_list_by_id($row->list_id)->title,0,10); ?></span>
																										<span>(<?php echo date("F j, Y",strtotime($checkin)).' - '.date("F j, Y",strtotime($checkout)) ?>)</span></p>
																										
																						<?php } ?>
		   </div>
                        </div>
                      
                        <div class="span3">
                        	
																								
																								<div class="clsMsg_Del clsFloatLeft msg_del">
																								
																								<?php if($row->is_starred == 0) $class = "clsMsgDel_Unfil"; else $class = "clsMsgDel_fil"; ?>
                    	     <p class="clearfix">
																										<span><a class="<?php echo $class; ?>" id="starred_<?php echo $row->id; ?>" href="javascript:void(0);" onclick="javascript:starred('<?php echo $row->id; ?>');"></a></span>
																										<span><a onclick="javascript:deleted('<?php echo $row->id; ?>');" href="javascript:void(0);" id="delete_<?php echo $row->id; ?>"><?php echo translate("Delete"); ?></a></span>
																										</p>
                        </div>
                           <div class="clsMeg_Off clsFloatLeft">
                              <p>
                              	<span><?php echo $row->name; ?></span>
																															<?php if($row->price != '') {?>
																															
																															<span><?php echo get_currency_symbol($row->list_id).get_currency_value1($row->list_id,$row->price); ?></span> 
																															<?php } ?>
                          </p>
                        </div>
                        
                        
                        </div>
                    </li>
				<h1 class="span12" style="background-repeat: no-repeat; background-position: center;"></h1>
																		
															<?php } } else { ?>
															
																		<li class="clearfix">
																					<?php echo translate("Nothing to show you."); ?>
																		</li> 
																					
															<?php } ?>
			</div></div>													
            </ul>
            <div style="clear:both"></div>
       </div>
       </div>
       </div>
       
    
  </div>
</div>  
</div>
</div>

</div>
<script type="text/javascript">

	$( "h1" ).filter( ":even" ).css({ 'background-image': 'url(<?php echo base_url(); ?>images/even_img.png)'});
	</script>
	<script type="text/javascript">
		$( "h1" ).filter( ":odd" ).css({ 'background-image': 'url(<?php echo base_url(); ?>images/odd_img.png)'});
	</script>
	
<script type="text/javascript">
function starred(id)
{

var className = $('#starred_'+id).attr('class');

	if(className == 'clsMsgDel_Unfil')
	{
	$("#starred_"+id).removeClass("clsMsgDel_fil").addClass("clsMsgDel_fil");
	var to_change = 1;
	}
	else
	{
	$("#starred_"+id).removeClass("clsMsgDel_fil").addClass("clsMsgDel_Unfil");
	var to_change = 0;
	}
	
	$.ajax({
				 type: "POST",
					url: "<?php echo site_url('message/starred'); ?>",
					async: true,
					data: "message_id="+id+"&to_change="+to_change,
					success: function(data)
		  	{	
					$("#message").html(data);
					$("#message").show();
					$("#message").delay(1000).fadeOut('slow');
			 	}
		  });
}

function deleted(id)
{
  var ok=confirm("Are you sure to delete the message?");
		if(!ok)
		{
			return false;
		}
	$.ajax({
				 type: "POST",
					url: "<?php echo site_url('message/delete'); ?>",
					async: true,
					data: "message_id="+id,
					success: function(data)
		  	{	
					$("#message").html("<?php echo translate('Message deleted successfully.');?>");
					$("#message").show();
					$("#message").delay(1000).fadeOut('slow');
					
					$("#messages_list").html(data);
			 	location.reload(); 
				}
		  });
}

function is_read(id)
{
	$.ajax({
				 type: "POST",
					url: "<?php echo site_url('message/is_read'); ?>",
					async: true,
					data: "message_id="+id
		  });
}

</script>