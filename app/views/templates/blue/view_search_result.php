<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link href="<?php echo css_url().'/jquery_colorbox.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo css_url().'/search_result.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<div class="container-fluid">
 <style type="text/css">
 body { font: normal 10pt Helvetica, Arial; }
 #map { width: 350px; height: 300px; border: 0px; padding: 0px; }
ui-slider-handle:focus
 {
 	background-color:#89C37A;
 }
 .SerPar_Inp_Bg input[type="text"]
 {
 	width:100% !important;
 }
 #location
 {
 	padding-right:30px !important;
 }

 </style>
<?php
$this->session->set_userdata('checkin','');
$this->session->set_userdata('checkout','');
$this->session->set_userdata('no_of_guest','');
?>

<?php $zz=0; ?>

<script type="text/javascript">

function show()
{

var location =  document.getElementById('location').value;
		var dataString = "&location=" +location;
			
	
	 b_url = "<?php echo base_url().'search/sample'?>";
		 $.ajax({
		   type: "GET",
		   url: b_url,
		   data: dataString,
		   success: function(data){
		   		$('#neighbor').html(data);
				   }
		 });
	
}


</script>
 <!---Include Validation for the Book it button----->
          <script type="text/javascript">
 $('#book_it_button').live('click',function()
  {
  	
  	var hid = $(this).attr("name");
 var subtotal=$(this).attr("alt");
var checkin = $("#checkin").val();
	var checkout = $("#checkout").val();
	var guest = $("#number_of_guests").val();
			
	var dataString = "checkin=" +checkin +"&checkout="+checkout + "&guest="+guest+"&subtotal=" +subtotal; 
	var c1= encodeURIComponent(checkin);
var c2=encodeURIComponent(checkout);
if($('#checkin').val()=='mm/dd/yy' && $('#checkout').val()=='mm/dd/yy')
{
	alert("Please choose the dates");

	return false;
}   
else
{  
  window.location.href="<?php echo base_url(); ?>payments/index/"+hid+"?"+dataString;
      }
     
    });
    
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $ ('ul.image_list li:even').addClass ('even');
       $ ('ul.image_list li:odd').addClass ('odd');
    });
</script>

<style>
</style>
<!---- End of script------->
<div id="Search_Main" class="list_view condensed_header_view" style="margin:2%">
	<div class="row-fluid">
		<div class="span12">
<!-- search_header -->
<div id="Selsearch_params" class="span12"> 
  <form onsubmit="clean_up_and_submit_search_request(); return false;" id="search_form" style="border:none;margin:0px;">
    <div class="SerPar_Inp_Bg span10">
    	
       <!-- <label for="location" class="inner_text" id="location_label" style="display:none;"><?php echo translate("City, address, or zip code"); ?></label>
	    <input type="text" autocomplete="off" id="location" name="location" />-->
		<input id="location" class="location span12" type="text" value="<?php echo translate("Zip, Address, Neighborhood, or Type"); ?>" name="location" autocomplete="off" onblur="if (this.value == ''){this.value = '<?php echo translate("Zip, Address, Neighborhood, or Type"); ?>'; }"
   onfocus="if (this.value == '<?php echo translate("Zip, Address, Neighborhood, or Type"); ?>') {this.value = ''; }" placeholder="<?php echo translate("Zip, Address, Neighborhood, or Type");?>" style="padding-right:30px;">
    </div>
    
    
    <div class="clsSer_Par_LocButt span2">
        <input class="blue_home span12" type="submit" id="submit_location" name="submit_location" style="float:right;margin:0px; padding:13px 0px;" value="<?php echo translate("Search");?>" onclick="show();"/>
        <input type="hidden" name="page" id="page" value="<?php echo $page; ?>" />
    </div>
    <div style="clear:both"></div>
  </form>
</div>
<!--Filters -->

       
       <!-- <div id="bed_room_filter">
        	<p>
            	<span><?php echo translate("Bedrooms:"); ?></span>
            <select name="min_bedrooms" id="min_bedrooms">
			   <option value=""><?php echo translate("None"); ?></option>
          	    <?php for($i = 1; $i <= 10; $i++) { ?>
			    <option value="<?php echo $i; ?>"><?php echo $i; ?> <?php echo translate('bedroom');?><?php if($i > 1) echo 's'; ?></option>
				<?php } ?>
			</select>
			   </select>
			   <label id="results_count_top"></label>
            </p>
       </div>-->

</div>
<!--Filters End-->	
<!-- search_params -->
<div id="standby_action_area" style="display:none;">
  <div> <b><a id="standby_link" href="/messaging/standby" target="_blank">
    <?php echo translate("Do you need a place <i>pronto</i>? Join our Standby list!"); ?>
    </a></b> </div>
</div>
<!-- search_body -->
<div id="search_filters_wrapper" class="search-main-right span3">
	<div id="search_filters">
	<!-- this partial is wrapped in a div class='search_filters' -->
	<!-- Map Container Hidden-->
         <!--   <div id="map_options"><input type="checkbox" name="redo_search_in_map" id="redo_search_in_map" /><label for="redo_search_in_map"><?php echo translate("Redo search in map"); ?></label></div>-->
         
         
           <div id="map_wrapper">
           	<div class="row-fluid">
            	<p class="zoom_info"><?php echo translate("Location");?></p>
            	<div  id="Mab_Big_Main" style="margin-top:-2px">
                   
    				<div id="search_map"></div>
                  
            	</div>	
            	
            	
                <div id="map_view_loading" class="rounded_more" style="display:none;"><img src="<?php echo base_url(); ?>images/page2_spinner.gif" style="display:block; float:left; padding:0 12px 0 0;"/><?php echo translate("Loading"); ?>...</div>
                <div id="map_message" class="span6" style="display:none;"></div>
                <div id="first_time_map_question" style="display:none;">
                    <div id="first_time_map_question_content" class="rounded">
                        <div id="first_time_map_question_arrow"></div>
                        <p><?php echo translate("Check this box to see new search results as you move the map"); ?>.</p>
            
                        <a id="redo_search_in_map_link_on" href="javascript:void(0);"><?php echo translate("Yes, please"); ?></a>
                        <a id="redo_search_in_map_link_off" href="javascript:void(0);"><?php echo translate("No, thanks"); ?></a>
                    </div>
                </div>
               </div>
            </div>
            
            
          <div id="property_type_filter">
        	
            	<h3><?php echo translate("Property Type"); ?></h3>
            <select name="property_type" id="property_type"  class="span10">
			   <option value="1"><?php echo translate("All"); ?></option>
                 <?php 
            			foreach($property_type as $property)
  						{
						  	echo " <option value='".$property['id']."'>".$property['type']."</option>";
						}
   				?>
			</select>
         
        </div>
        <li id="price_container" class="search_filter" style="border:none;">
                <div class="Box searchpage_Box">
                    <div class="Box_Head searchhead">
                       <a class="filter_toggle"></a>
                       <h2><a href="javascript:void(0);" class="filter_header"><?php echo translate("Price"); ?></a></h2>
                    </div>
                    <div class="search_filter_conten">
                       
                        <ul id="slider_values">
                            <li id="slider_user_min" class="span2" style="margin-right:9px">$10</li>
                            <li style="padding:0px 13px"> <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all span7"><div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></a></div></li>
                            <li id="slider_user_max" class="span3" style="float:right">$10000+ </li>
                        </ul>
                    </div>
                </div>
            </li>
              <ul class="collapsable_filters">
            	
            <li class="search_filter" id="room_type_container">
                <div class="Box searchpage_Box">
                    <div class="Box_Head searchhead">
                         
                         <h2 style="margin-left:0px;"><a class="filter_header" href="javascript:void(0);"><?php echo translate("Size"); ?></a>
                         <a class="filter_toggle"></a>
                         </h2>
                  	</div>
                    <!-- Search filter content is below this -->
                    <ul class="search_filter_content">
                        <li class="clearfix">
                          <input type="checkbox" value="Small" name="room_types" class="room_0 regular-checkbox big-checkbox" id="room_type_0" <?php
						if(isset($room_types1)){
						if($room_types1 == 'Small')
						{
							echo "checked";
						}
						}
						?>	>
						<label for="room_type_0"></label>
                        <label for="room_type_0" class="check_align"> <?php echo translate("Small ( Less than 5' X 5' )"); ?> </label>
                        </li>
						
                        <li class="clearfix">
                            <input type="checkbox" value="Medium" name="room_types" class="regular-checkbox big-checkbox" id="room_type_1" <?php
						if(isset($room_types2)){
						if($room_types2 == 'Medium')
						{
							echo "checked";
						}
						}
						?>>
						<label for="room_type_1"></label>
                        <label for="room_type_1" class="check_align"> <?php echo translate("Medium ( approx . 5' X 5' )"); ?> </label>
                        </li>
                        <li class="clearfix">
                            <input type="checkbox" value="Large" name="room_types" class="regular-checkbox big-checkbox" id="room_type_2" <?php
						if(isset($room_types3)){
						if($room_types3 == 'Large')
						{
							echo "checked";
						}
						}
						?> >
						<label for="room_type_2"></label>
                            <label for="room_type_2" class="check_align"><?php echo translate("Large ( approx . 10' X 10' )"); ?></label>
                        </li>
                        <li class="clearfix">
                        	<input type="checkbox" value="X-Large" name="room_types"class="regular-checkbox big-checkbox" id="room_type_3" <?php
						if(isset($room_types4)){
						if($room_types4 == 'X-Large')
						{
							echo "checked";
						}
						}
						?> >
						<label for="room_type_3"></label>
						<label for="room_type_3" class="check_align"><?php echo translate("X-Large (Parking space or Larger than 10' X 10' )"); ?></label></li>
                       <!-- <li class="Txt_Right_Align"><a class="show_more_link" href="javascript:void(0);"><?php echo translate("Show More"); ?>...</a></li>-->
                    </ul>
                    <div style="clear:both"></div>
            	</div>
              <!-- End of search filter content -->
            </li>
            <li style="clear:both;"></li>
             <li class="search_filter">
                <div class="Box searchpage_Box">
                    <div class="Box_Head searchhead">
                        <a class="filter_toggle"></a>
                        <h2><a href="javascript:void(0);" class="filter_header"><?php echo translate("Access Type"); ?></a></h2>
                    </div>
                    <div class="search_filter_content">
                        <ul>
                          <!--  <li><input type="checkbox" id="cpr" class="regular-checkbox big-checkbox" name="checkbox1"></input>
                          	<label for="cpr"></label>
                            	<label>Limited Access</label></li>
                            <li><input type="checkbox" name="checkbox1" class="regular-checkbox big-checkbox"></input>
                            	<label for="cpr" style="margin-bottom:2px;"></label>
                            	<label>Any time Access</li> -->
                            		
                            		   <li class="clearfix">
                        <input type="checkbox" value="1" name="amenities" id="amenity_0" class="regular-checkbox big-checkbox"> 
                        <label for="amenity_0"></label>
                        <label for="amenity_0"><?php echo translate("Limited Access"); ?></label>
                        </li>
                        <li class="clearfix">
                        <input type="checkbox" value="2" name="amenities" id="amenity_1" class="regular-checkbox big-checkbox"> 
                        <label for="amenity_1"></label>
                        <label for="amenity_1"><?php echo translate("Any time Access"); ?></label>
                        </li>
                            		
                            	</label>
                        </ul>
                    </div>
                </div>
            </li>
            
            
          <!--  <li id="amenities_container" class="search_filter closed">
                <div class="Box searchpage_Box">
                    <div class="Box_Head searchhead">
                        <a class="filter_toggle"></a>
                        <h2><a href="javascript:void(0);" class="filter_header"><?php echo translate("Amenities"); ?></a></h2>
                    </div>
                    <ul class="search_filter_content">
                        <li class="clearfix">
                        <input type="checkbox" value="1" name="amenities" id="amenity_0"> 
                        <label for="amenity_0"><?php echo translate("Smoking Allowed "); ?></label>
                        </li>
                        <li class="clearfix">
                        <input type="checkbox" value="2" name="amenities" id="amenity_1"> 
                        <label for="amenity_1"><?php echo translate("Pets Allowed"); ?></label>
                        </li>
                        <li class="clearfix">
                        <input type="checkbox" value="7" name="amenities" id="amenity_2"> 
                        <label for="amenity_2"><?php echo translate("Elevator in Building "); ?></label>
                        </li>
                        <li class="Txt_Right_Align">
                        <a class="show_more_link" href="javascript:void(0);"><?php echo translate("Show More"); ?>...</a>
                        </li>
                    </ul>
                </div>
            </li>		-->		
            
			<li id="search_filter" class="search_filter closed">
                <div class="Box searchpage_Box">
                    <div class="Box_Head searchhead">
                        <a class="filter_toggle"></a>
                        <h2><a href="javascript:void(0);" onclick="show()" class="filter_header"><?php echo translate("Available Dates"); ?></a></h2>
					</div>
					<ul style="margin-left:0px;">
					<li class="clearfix">
					<div class="row-fluid">
						<div id="SelSer_Par_Inps" class="span12">
      <div class="dates_section span6">
        <div class="heading">
          <?php echo translate("Start Date"); ?>
        </div>
        <input id="checkin" class="checkin date active span12" name="checkin" autocomplete="off" value="Check In" readonly/>
      </div>
      <div class="dates_section span6">
        <div class="heading">
          <?php echo translate("End Date"); ?>
        </div>
        <input id="checkout" class="checkout date active span12" name="checkout" autocomplete="off" value="Check Out" readonly/>
      </div>
   <!--   <div class="guests_section">
        <div class="heading">
          <?php echo translate("Guests"); ?>
        </div>
        <select id="number_of_guests" name="number_of_guests" value="Guests">
		 
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16+</option>
        </select>
   </div>-->
    
	                   </div>
	                  </div>
					</li>
					</ul>
						
			     </div>
            </li>			

			<li id="amenities_container" class="search_filter closed">
                <div class="Box searchpage_Box" style="border:0px" >
                    <div class="Box_Head searchhead">
                        <a class="filter_toggle1"></a>
                        <h2><a href="javascript:void(0);" onclick="show()" class="filter_header"><?php echo translate("Neighborhood"); ?></a></h2>
					</div>
					<ul class="search_filter_content sear_neigh">
					<li class="clearfix">
					<div id="neighbor">
											
					
					</div>
					</li>
					</ul>
						
			     </div>
            </li>			
			<li id="keywords_container" class="search_filter closed">
            <div class="Box searchpage_Box" style="border:0px">
             <div class="Box_Head searchhead">
                	
                    <a class="filter_toggle1"></a>
                    <h2><a href="javascript:void(0);" class="filter_header"><?php echo translate("Keywords"); ?></a></h2>
                </div>
                <div class="row-fluid">
                	<div class="span12" style="background-color:white;">
                <ul class="search_filter_content sear_neigh">
                    <li class="clearfix" style="margin-top:10px">
                        <input type="text" id="keywords" name="keywords" defaultvalue="Enter Keywords" class="active span12">
                        <a href="#" onclick="clean_up_and_submit_search_request(); return false;" id="submit_keyword"></a>
                    </li>
                </ul>
                </div>
                </div>
           </div>
            </li>
                
            
            </ul>
<div id="small_map_loading" class="opacity_80 rounded" style="display:none; border:2px solid #989898; -moz-box-shadow:0 0 2px #A8A8A8; -webkit-box-shadow:0 0 2px #A8A8A8;"><img src="<?php echo base_url(); ?>images/page2_spinner.gif" style="width:16px; height:16px; display:block; float:left;"/>
  <?php translate("Loading...",$this->session->userdata('lang'));?>
</div>
<div id="search_filters_toggle" class="search_filters_toggle_on rounded_left"></div>
</div>
</div>
<div class="search-main-left span9">
	<div class="row-fluid">
		<div class="span12">
				<div id="search_new_filters">
         <div id="sort_by_filter">
        	<p class="span2">
            	<?php echo translate("Sort by"); ?>:
            	
         	<div class="span8">
                  <select name="sort" id="sort" class="span12">
			   <option value="1"><?php echo translate("Recommended"); ?></option>
               <option value="2"><?php echo translate("Price: low to high"); ?></option>
               <option value="3"><?php echo translate("Price: high to low"); ?> </option>
               <option value="4"><?php echo translate("Newest"); ?></option>
			   </select>
        
    		</p>
        
        </div>
          
             <div class="search_type_option" id="search_type_short" onclick="view_shortlist(this)">
 				<input type="hidden" id="short" value="short">
          <button class="My_list span2"><?php echo translate("My Wishlist"); ?></button>
    	</div>
       </div>
     </div>
     </div>
     <div class="row-fluid">
     	<div class="span12">
<div class="Box_Head clearfix">
  		<!-- Left -->   
        <div id="search_type_toggle">
            <div class="search_type_option search_type_option_active" id="search_type_list">
              <span><?php echo translate("List"); ?></span>
            </div>
         	<div class="search_type_option clsBorder_No" id="search_type_map"><span><?php echo translate("Map"); ?></span></div>
 			 
    </div>
    	<!-- End of Left -->
        <!-- Right -->
        <!-- End of Right -->
         <!-- <div class="social_links">
  <!-- AddThis Button BEGIN -->
  <!--  <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_counter addthis_pill_style"></a>
    </div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5202fbea486f356f"></script>
<!-- AddThis Button END -->
 <!-- </div>-->
    </div>
    </div>
   </div>
<div style="clear:both"></div>   
<div class="row-fluid">
	<div class="span12">
<div id="search_body" class="Box">
    <!-- Results header was here initially -->
    <!--  End of results header -->
    <div id="results_filters" style="">
        <div id="filters_text"><?php echo translate("Filters:"); ?></div>
        <ul id="applied_filters">
        </ul>
      </div>
    <ul id="results" class="image_list"style="margin:0px;">
      </ul>
    <!-- results -->
   <!-- <div id="results_footer" class="clearfix"style="display:none;">
        <div class="results_count clsFloatLeft"></div>
        <div id="results_pagination" class="clsFloatRight"></div>
   </div>-->
    <!-- results_footer -->
    <div id="list_view_loading" class="rounded_more" style="display:none;"> <img src="<?php echo base_url(); ?>images/page2_spinner.gif" style="vertical-align: middle;" height="42" width="42" alt="" /> </div>
</div>
</div>
</div>
</div>
</div>
<!--End Of search_body -->
<!-- Contents below this is for the search filters -->

</div>
<!-- v3_search -->

<ul id="blank_state_content" style="display:none;">
  <li id="blank_state">
    <div id="blank_state_molecule"></div>
    <div id="blank_state_text">
      <h3>
        <?php echo translate("Your search was a little too specific."); ?>
      </h3>
      <p style="padding:15px 0 0 5px;">
        <?php echo translate("We suggest unchecking a couple of filters, or searching for a different city."); ?>
      </p>
    </div>
  </li>
</ul>

							

<style type="text/css">
.ac_results { border-color:#a8a8a8; border-style:solid; border-width:1px 2px 2px; margin-left:1px; }
</style>
<script type="text/x-jqote-template" id="badge_template">
    <![CDATA[
        <li class="badge badge_type_<*= this.badge_type *>">
            <span class="badge_image">
                <span class="badge_text"><*= this.badge_text *></span>
            </span>
            <span class="badge_name"><*= this.badge_name *></span>
        </li>
    ]]>
</script>
<script type="text/x-jqote-template" id="list_view_item_template">
    <![CDATA[
    
        <li id="room_<*= this.hosting_id *>" class="search_result span12" style="margin-left:0px">
                	<div class="span4">
            <div class="pop_image_small>
                <div class="map_number"><*= this.result_number *></div>
                <ul class="enlarge"> 
                <a href="<?php echo base_url(); ?>rooms/<*= this.hosting_id *>" class="image_link" title="<*= this.hosting_name *>">
                	<li>
                	<img alt="<*= this.hosting_name *>" class="search_thumbnail" height="426" src="<*= this.hosting_thumbnail_url *>" title="<*= this.hosting_name *>" width="100%" />
				<!--<span>
					<?php 
					$id = '<*= this.hosting_id *>';
					$image_url = '<*= this.hosting_thumbnail_url*>';
					$image_url_wm = $image_url.'_watermark.jpg';
					?>
	<img alt="<*= this.hosting_name *>" id="img_wm" class="search_thumbnail_wm" height="230" src="<?php echo $image_url_wm; ?>" title="<*= this.hosting_name *>" width="400" />
		<br /><br /><p class="pricemm"><*= this.hosting_name *></p>
		
</span>-->
<div class="search_shadow">

</li>
				</a>
				 	
							</ul>
            </div>
           

           </div>
                <!-- Add bookit button for all aparments -->
    
<!--<input  name="hosting_id" type="hidden" value="<*= this.hosting_id *>" />
			<a class="btn green bookit_button" href="#" alt="<*=this.price*>" name="<*= this.hosting_id *>" id="book_it_button" style="width:100px !important;">Book it</a>
<!-----End of it------->

	<div class="span8" style="margin-bottom:15px">
            <div class="room_details">
                <h2 class="room_title">
                  <a class="name" href="<?php echo base_url(); ?>rooms/<*= this.hosting_id *>"><*= this.hosting_name *></a>
                 <a href="#" id="star_<*= this.hosting_id *>" title="Add this listing as a 'favorite'" class="star_icon_container"><div class="star_icon"></div></a>
                </h2>
                <p>Size , <*= this.room_type *> , <span><*= this.city *> City</span></p>
                <p style="font-family:cantarell;">Rate : <*= this.symbol *><*= this.price *>/day <span class="price1">Per Month<span style="color:red;padding-left:10px;"> <*= this.symbol *><*= this.price *></span></span></p>
           <* if(this.distance) { *>
                    <!--<p class="address_max_width"><*= this.address *></p>-->
                   <!-- <p class="distance"><*= this.distance *> <*= Translations.distance_away *></p>-->
                <* } else { *>
                  <!--  <p class="address"><*= this.address *></p>-->
                <* } *>
				<!--<ul class="reputation"> <a href="<?php echo base_url(); ?>users/profile/<*= this.user_id *>"><img alt="<*= this.user_name *>" height="36" src="<*= this.user_thumbnail_url *>" title="<*= this.user_name *>" width="36" /></a> </ul>-->
            
            <div class="price">
                <div class="price_data">
                  <p class="currency_if_required"><*= CogzidelSearch.currencySymbolRight *>
                  <!--  <?php 
                    $xml = '<?xml version="1.0" encoding="UTF-8" ?>
<rss>
    <channel>
        <item>
            <title><![CDATA[<*= this.hosting_id *>]]></title>
        </item>
    </channel>
</rss>';

$xml = simplexml_load_string($xml);
$hosting_id = $xml->channel->item->title;
$hosting_id = (string)$hosting_id;
//$hosting_id = (int)$hosting_id;
var_dump(trim($hosting_id));
//echo get_currency_symbol($hosting_id);
 ?>-->
 
                   <!-- <div class='currency_with_sup1'><*= this.symbol *></p><*= this.price *></div>-->
                </div>
              <!--  <div class="price_modifier">
                    Per day
              </div>-->
            </div>
            </div>
             <p class="search_para"><*= this.desc *></p>
			<div class="user_thumb">
          </div>
          </div>
         

       <div class="row-fluid">
       	<div class="span12">
       		<div class="span4"></div>
					<!--<table width="90%" cellspacing="0" cellpadding="0" border="0" style="padding:10px 0 0; margin:0;">-->
  <!--<tbody><tr>-->
  <!--  <td width="65%" valign="middle" align="right" style="float:right; padding-right:10px; padding-top:10px;">-->
  	<div class="span8">
  		<div class="row-fluid">
  			<div class="span12">
  				<div class="span5">
		<div style="float:left;"  class="count_badge">
		</div><div style="float:left; padding:3px 5px; color:#62AF4F;line-height:normal;font-size:18px;font-family: padaukregular;"><*= this.views*></div>
			<div style="float:left;padding:4px 0 0 5px;color:#62AF4F;line-height:normal;font-size:18px;font-family: padaukregular;"><?php echo translate('Reviews');?></div></div><!--</td>-->
		<!--<td width="25%" valign="middle" align="center">--> 
			<div class="span4">
			<div class="addshortlist span12">
    	<* if(this.short_listed == 1) { *>
				<a href="#"><input class="accept_button1 span12" oncontextmenu="return false" type="button" value="<?php echo translate("Saved to Wish List"); ?>" id="my_shortlist" onclick="add_shortlist(<*= this.hosting_id *>,this);"></a>
				<* } else { *>
				<a href="#"><input class="accept_button1 span12" oncontextmenu="return false" type="button" value="<?php echo translate("Save To Wish List"); ?>" id="my_shortlist" onclick="add_shortlist(<*= this.hosting_id *>,this);"></a>
				<* } *>
				</div>
				</div>
  <!-- </td>-->
   <!-- <td width="10%" valign="middle" align="center">-->
   	<div class="span3">
		<a class="span12" href="#" alt="<*=this.price*>" name="<*= this.hosting_id *>" id="book_it_button" oncontextmenu="return false" style="display: inline-block;font-size:20px;border-radius:5px;background-color:#333333; padding:7.1px 5px;color:white; text-align: center;margin: 0px; text-transform: none;"><?php echo translate('Book It');?></a>
	</div>
	<!--</td>-->
  <!--</tr>-->

<!--</tbody>
</table>-->
</div>
</div>
</div>
</div>

</div>

  <!--  <div class="page_viewed_count"><*= this.views *></div><div class="views"> <?php echo translate("Views");?></div>
			<div class="addshortlist">
				<* if(this.short_listed == 1) { *>
				<a href="#"><input class="accept_button" type="button" value="<?php echo translate("Saved to Wish List"); ?>" id="my_shortlist" onclick="add_shortlist(<*= this.hosting_id *>,this);"></a>
				<* } else { *>
				<a href="#"><input class="accept_button" type="button" value="<?php echo translate("Save To Wish List"); ?>" id="my_shortlist" onclick="add_shortlist(<*= this.hosting_id *>,this);"></a>	
				<* } *>
			</div>-->
			<* if (this.connections.length > 0) { *>

			<div class="room-connections-wrapper">
				<span class="room-connections-arrow"></span>
				<div class="room-connections">
					<ul>
						<* for (var k = 0; k < Math.min(this.connections.length, 3); k++) { *>
						<li>
							<img height="28" width="28" alt="" src="<*= this.connections[k].pic_url_small *>" />
							<div class="room-connections-title">
								<div class="room-connections-title-outer">
									<div class="room-connections-title-inner">
										<*= this.connections[k].caption *>
									</div>
								</div>
							</div>
						</li>
						<* } *>
					</ul>
				</div>
			</div>
			<* } *>
        </li>
          ]]>
</script>


	
<script type="text/x-jqote-template" id="applied_filters_template">
    <![CDATA[
        <li id="applied_filter_<*= this.filter_id *>"><span class="af_text"><*= this.filter_display_name *></span><a class="filter_x_container"><span class="filter_x"></span></a></li>
    ]]>
</script>
<script type="text/x-jqote-template" id="list_view_airtv_template">
    <![CDATA[
        <div id="airtv_promo">
            <img src="/images/page2/v3/airtv_promo_pic.jpg" />
            <h2><*= this.airtv_headline *></h2>
            <h3><*= this.airtv_description *> <b><?php echo translate("Watch Now!");?></b></h3>
        </div>
    ]]>
</script>

<div style="display: none">
  <div id="filters_lightbox">
      <ul id="filters_lightbox_nav" class="Box_Head">
          <li class="filters_lightbox_nav_element" id="lightbox_nav_room_type"><a href="javascript:void(0);"><?php echo translate("Property"); ?></a></li>

          <li class="filters_lightbox_nav_element" id="lightbox_nav_amenities"><a href="javascript:void(0);"><?php echo translate("Amenities"); ?></a></li>
      </ul>

      <ul id="lightbox_filters">
          <li class="lightbox_filter_container" id="lightbox_container_room_type" style="display:none;">
              <div class="lightbox_filters_left_column">

            <h3> Room Type</h3>
                 <ul class="search_filter_content" id="lightbox_filter_content_room_type">
					<li class="clearfix">
					<input type="checkbox" value="Entire home/apt" name="room_types" id="lightbox_room_type_0" <?php
						if(isset($room_types1)){
						if($room_types1 == 'Entire home/apt')
						{
							echo "checked";
						}
						}
						?>	> 
					<label for="lightbox_room_type_0"><?php echo translate("Entire home/apt"); ?></label>
					</li>
					<li class="clearfix">
					<input type="checkbox" value="Private room" name="room_types" id="lightbox_room_type_1" <?php
						if(isset($room_types2)){
						if($room_types2 == 'Private room')
						{
							echo "checked";
						}
						}
						?>>
					<label for="lightbox_room_type_1"><?php echo translate("Private room"); ?></label>
					</li>
					<li class="clearfix">
					<input type="checkbox" value="Shared room" name="room_types" id="lightbox_room_type_2" <?php
						if(isset($room_types3)){
						if($room_types3 == 'Shared room')
						{
							echo "checked";
						}
						}
						?> >
					<label for="lightbox_room_type_2"><?php echo translate("Shared room"); ?></label>
					</li>
				</ul>
                  <h3><?php echo translate("Size"); ?></h3>
                  <ul id="lightbox_filter_content_size" class="search_filter_content">
                      <li>
                          <label for="min_bedrooms"><?php echo translate("Min Bedrooms"); ?></label>
                          <select class="dropdown" id="min_bedrooms" name="min_bedrooms"><option value=""></option>
						<?php for($i = 1; $i <= 10; $i++) { ?>
									<option value="<?php echo $i; ?>"><?php echo $i; ?> bedroom<?php if($i > 1) echo 's'; ?></option>
							<?php } ?>
						</select>
  					</li>

                      <li>
                          <label for="min_bathrooms"><?php echo translate("Min Bathrooms"); ?></label>
                          <select class="dropdown" id="min_bathrooms" name="min_bathrooms"><option value=""></option>
								<?php for($i = 1; $i <= 10; $i++) { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?> bathroom<?php if($i > 1) echo 's'; ?></option>
									<?php } ?>
								</select>
                      </li>
                      <li>

                          <label for="min_beds"><?php echo translate("Min Beds"); ?></label>
                          <select class="dropdown" id="min_beds" name="min_beds">
							<option value=""></option>
							<?php for($i = 1; $i <= 16; $i++) { ?>
													<option value="<?php echo $i; ?>"><?php echo $i; if($i == 16) echo '+'; ?> <?php echo translate("bed"); ?></option>
								<?php } ?>
							</select>
                      </li>
                  </ul>
              </div>
              <div class="lightbox_filters_right_column">
                  <h3><?php echo translate("Property Type"); ?></h3>
                 <ul class="search_filter_content" id="lightbox_filter_content_property_type_id">
					 <?php 
					 echo '<li class="clearfix">';
					$property = $this->db->from('property_type')->get();
					foreach($property->result() as $value) {  ?>
					<input type="checkbox" value="<?php echo $value->type;?>" name="property_type_id" id="lightbox_property_type_id_<?php echo $value->type;?>">
					<label for="lightbox_property_type_id_<?php echo $value->type;?>"><?php echo $value->type.'<br>'; ?></label>
					<?php } echo '</li>';?>
					
				</ul> 
              </div>

          </li>
          


          <li class="lightbox_filter_container" id="lightbox_container_amenities" style="display:none;">
										
												<?php 
											 	$tCount = $amnities->num_rows();
												 $j = 1; 
													echo '<ul class="search_filter_content">'; 
													foreach($amnities->result() as $rows) {  ?>
                    <li>
                   <input type="checkbox" name="amenities" id="lightbox_amenity_<?php echo $j; ?>" value="<?php echo $j; ?>">
																						<label for="lightbox_amenity_<?php echo $j; ?>"><?php echo $rows->name; ?></label>
                    </li>
													<?php $j++; }  echo '</ul>'; ?> 
										
          </li>

          <li class="lightbox_filter_container" id="lightbox_container_host" style="display:none;">
              <div class="lightbox_filters_left_column">

                  <h3><?php echo translate("Languages Spoken"); ?></h3>
                  <ul id="lightbox_filter_content_languages" class="search_filter_content"></ul>
              </div>
              <div class="lightbox_filters_right_column">

              </div>

              <ul class="search_filter_content"></ul>
          </li>


      </ul><!-- lightbox_filters -->

      <div id="lightbox_filter_action_area" class="rounded_bottom">
          <a href="javascript:void(0);" onclick="SearchFilters.closeFiltersLightbox();"><?php echo translate("Cancel"); ?></a>

          <button id="lightbox_search_button" name="Search" class="btn blue gotomsg" type="submit"><span><span><?php echo translate("Yes"); ?></span></span></button>
      </div>
  </div><!-- filters_lightbox -->
</div>
</div>
</div>
</div>


	
<script type="text/javascript">

$(document).ready(function () {
	
      var input = document.getElementById('location');
    autocomplete = new google.maps.places.Autocomplete(input);    
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    	
    });
    
});



</script>
<script type="text/javascript">

/*if ((navigator.userAgent.indexOf('iPhone') == -1) && (navigator.userAgent.indexOf('iPod') == -1) && (navigator.userAgent.indexOf('iPad') == -1)) {
    jQuery(window).load(function() {
        LazyLoad.js([
			"<?php //echo base_url().'js' ?>/jquery.autocomplete_custom.pack.js",
			"<?php //echo base_url().'js' ?>/ en_autocomplete_data.js"],
			function() {
            	jQuery("#location").autocomplete(autocomplete_terms, {
	                minChars: 1, width: 322, max:20, matchContains: false, autoFill: true,
	                formatItem: function(row, i, max) {
	                    //to show counts, uncomment
	                    return Cogzidel.Utils.decode(row.k);
	                },
	                formatMatch: function(row, i, max) {
	                    return Cogzidel.Utils.decode(row.k);
	                },
	                formatResult: function(row) {
	                    return Cogzidel.Utils.decode(row.k);
	                }
	            });
	        }
		);
    });
}*/
    jQuery(document).ready(function(){
        Cogzidel.Bookmarks.starredIds = [];

        CogzidelSearch.$.bind('finishedrendering', function(){ 
          Cogzidel.Bookmarks.initializeStarIcons(function(e, isStarred){ 
            // hide the listing result from the set of search results when the result is unstarred
            if(!isStarred && CogzidelSearch.isViewingStarred){
              if(CogzidelSearch.currentViewType == 'list')
                $('#room_' + $(e).data('hosting_id')).slideUp(500);
              else if(CogzidelSearch.currentViewType == 'photo')
                $('#room_' + $(e).data('hosting_id')).fadeOut(500);
            }
          }) 
        });

            SearchFilters.amenities.a_11 = ["Smoking Allowed", false];
            SearchFilters.amenities.a_12 = ["Pets Allowed", false];
            SearchFilters.amenities.a_1 = ["TV", false];
            SearchFilters.amenities.a_2 = ["Cable TV", false];
            SearchFilters.amenities.a_3 = ["Internet", false];
            SearchFilters.amenities.a_4 = ["Wireless Internet", false];
            SearchFilters.amenities.a_5 = ["Air Conditioning", false];
            SearchFilters.amenities.a_30 = ["Heating", false];
            SearchFilters.amenities.a_21 = ["Elevator in Building", false];


            SearchFilters.amenities.a_6 = ["Handicap Accessible", false];
            SearchFilters.amenities.a_7 = ["Pool", false];
            SearchFilters.amenities.a_8 = ["Kitchen", false];
            SearchFilters.amenities.a_9 = ["Parking Included", false];
            SearchFilters.amenities.a_13 = ["Washer / Dryer", false];
            SearchFilters.amenities.a_14 = ["Doorman", false];
            SearchFilters.amenities.a_15 = ["Gym", false];
            SearchFilters.amenities.a_25 = ["Hot Tub", false];
            SearchFilters.amenities.a_27 = ["Indoor Fireplace", false];
            SearchFilters.amenities.a_28 = ["Buzzer/Wireless Intercom", false];
            SearchFilters.amenities.a_16 = ["Breakfast", false];
            SearchFilters.amenities.a_31 = ["Family/Kid Friendly", false];
            SearchFilters.amenities.a_32 = ["Suitable for Events", false];

        //CogzidelSearch.currencySymbolLeft = '<?php //echo get_currency_symbol(1); ?>';
        CogzidelSearch.currencySymbolRight = "";
        SearchFilters.minPrice = 10;
        SearchFilters.maxPrice = 10000;
        SearchFilters.minPriceMonthly = 150;
        SearchFilters.maxPriceMonthly = 5000;

        var options = {};

        //Some More Testing needs to be done with this logic - there are still edge cases
        //here, we add ability to hit the back button when the user goes from (page2 saved search)->page3->(browser back button)
        if(CogzidelSearch.searchHasBeenModified()){
            options = {"location":"<?php echo $query; ?>","number_of_guests":"<?php echo $number_of_guests; ?>","action":"ajax_get_results","checkin":"<?php echo $checkin; ?>","guests":"<?php echo $number_of_guests; ?>","checkout":"<?php echo $checkout; ?>","submit_location":"Search","controller":"search"};
        } else {
            options = {"location":"<?php echo $query; ?>","number_of_guests":"<?php echo $number_of_guests; ?>","action":"ajax_get_results","checkin":"<?php echo $checkin; ?>","guests":"<?php echo $number_of_guests; ?>","checkout":"<?php echo $checkout; ?>","submit_location":"Search","controller":"search"};
        }

          CogzidelSearch.isViewingStarred = false;
       

        if(options.search_view) {
            CogzidelSearch.forcedViewType = options.search_view;
        }

        //keep translations first
        Translations.clear_dates = "Clear Dates";
        Translations.entire_place = "Entire Place";
        Translations.friend = "friend";
        Translations.friends = "friends";
        Translations.loading = "Loading";
        Translations.neighborhoods = "Neighborhoods";
        Translations.private_room = "Private Room";
        Translations.review = "review";
        Translations.reviews = "reviews";
        Translations.superhost = "superhost";
        Translations.shared_room = "Shared Room";
        Translations.today = "Today";
        Translations.you_are_here = "You are Here";
        Translations.a_friend = "a friend";
        Translations.distance_away = "away";
        Translations.instant_book = "Instant Book";
        Translations.show_more = "Show More...";
        Translations.learn_more = "Learn More";
        Translations.social_connections = "Social Connections";

        //these are generally for applied filter labels
        Translations.amenities = "Amenities";
        Translations.room_type = "Room Type";
        Translations.price = "Price";
        Translations.keywords = "Keywords";
        Translations.property_type = "Property Type";
        Translations.bedrooms = "Bedrooms";
        Translations.bathrooms = "Bathrooms";
        Translations.beds = "Beds";
        Translations.languages = "Languages";
        Translations.collection = "Collection";

        //zoom in to see more properties message in map view
        Translations.redo_search_in_map_tip = "\"Redo search in map\" must be checked to see new results as you move the map";
        Translations.zoom_in_to_see_more_properties = "Zoom in to see more properties";

        //when map is zoomed in too far
        Translations.your_search_was_too_specific = "Your search was a little too specific.";
        Translations.we_suggest_unchecking_a_couple_filters = "We suggest unchecking a couple filters, zooming out, or searching for a different city.";

        //Tracking Pixel
        //run after localization
        TrackingPixel.params.uuid = "yq0m0k6hjg";
        TrackingPixel.params.user = "";
        TrackingPixel.params.af = "";
        TrackingPixel.params.c = "";
        TrackingPixel.params.pg = '2';

        CogzidelSearch.init(options);

    });
    		$( ".span128" ).each(function(index) {

if((index % 2) == 0)
{
	$( this ).addClass("bg1");
}else{
	$( this ).addClass("bg2");
}

	

	
});
	jQuery(document).ready(function() {
		

		Cogzidel.init({userLoggedIn: false});
		//My Wish List Button-Add to My Wish List & Remove from My Wish List
		add_shortlist = function(item_id,that) {
		
		var value = $(that).val();
		if(value == "<?php echo translate("Save To Wish List"); ?>")	
		{
		$.ajax({
  				url: "<?php echo site_url('search/add_my_shortlist'); ?>",
  				async: true,
  				type: "POST",
  				data: "list_id="+item_id,
  				success: function(data) {
  				if(data == "error")
  				window.location.replace("<?php echo base_url(); ?>users/signin");
  				else
    			$(that).attr('value', '<?php echo translate("Saved to Wish List"); ?>'); 
  				}
   				});
   		}
   		else
   		{
   		$.ajax({
  				url: "<?php echo site_url('search/remove_my_shortlist'); ?>",
  				async: true,
  				type: "POST",
  				data: "list_id="+item_id,
  				success: function(data) {
  				if(data == "error")
  				window.location.replace("<?php echo base_url(); ?>users/signin");
  				else			
    			$(that).attr('value', '<?php echo translate("Save To Wish List"); ?>'); 
    			  				}
   				});   			
   		}			
    	};
    	//My Wish List Menu-Check whether the user is login or not 
    	view_shortlist =  function(that){
    			var value = $('#short').val();
    			if(value=="short")
    			{
    				$.ajax({
      				url: "<?php echo site_url('search/login_check'); ?>",
      				async: true,
      				success: function(data) {
      				if(data == "error")
      				window.location.replace("<?php echo base_url(); ?>users/signin");
      				else
      				{
      				$('#search_type_short').attr('id','search_type_photo');
      				$('#short').attr('value', 'photo');
      				$("#search_type_photo").trigger("click");
      				}
      				}
      				});
      			}
    	};	
    			
		});
</script>
<script>
	
	$(function() {
       var date = new Date();
var currentMonth = date.getMonth();
var currentDate = date.getDate();
var currentYear = date.getFullYear();
	   $( "#checkout" ).datepicker({
                minDate: 0,
                maxDate: "+2Y",
                nextText: "",
                prevText: "",
                numberOfMonths: 1,
                // closeText: "Clear Dates",
                currentText: Translations.today,
                showButtonPanel: true
	    });
	    $( "#checkin" ).datepicker({
			minDate: date,
                maxDate: "+2Y",
                nextText: "",
                prevText: "",
                numberOfMonths: 1,
                currentText: Translations.today,
                showButtonPanel: true,
	 onClose: function(dateText, inst) { 
          d = $('#checkin').datepicker('getDate');
		  d.setDate(d.getDate()+1); // add int nights to int date
		$("#checkout").datepicker("option", "minDate", d);
		setTimeout(function () {
                                    $("#checkout").datepicker("show")
                                }, 0)
     }
	   });
       
    });
</script>



	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	