<style>
	label
	{
		font-size:17px;
		color:black;
	}
	.cont{
		font-size:15px;
	}
	#email
	{
		 border-bottom-left-radius: 2px;
    border-bottom-right-radius: 2px;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    /*box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2) inset;*/
    border:1px solid #C0C0C0;
 }
 .button2
 {
 	background-color:#c0c0c0;
 	color:white;
 	font-size:15px;
 	padding:5px 7px;
 	background-image:none;
 }
 .button2:hover
 {
 	background-color:gray;
 }
.button2 span
{
	font-size:15px;
	font-weight:bold;
	text-transform: uppercase;
}
</style>
<div class="container">
<div id="View_Contact" class="container_bg">
<!-- BEGIN STATIC LAYOUT -->

<div class="Box">
    <div class="Box_Head">
        <h2><?php echo translate("Contact us"); ?></h2>
    </div>
<div class="Box_Content">
	<div class="row-fluid">
		<div class="span12">

<div class="clearfix" id="Contact_content">
<div class="span5">
			<div class="clsFloatLeft" id="Contact_Left">
				<div class="row-fluid">
					
							<div class="span4">	<label><?php echo translate("Phone Support"); ?></label></div>
									<div class="span5 cont"><?php if(isset($row->phone) && $row->phone != '') echo $row->phone; else echo "-"; ?></div>
							
						</div>
						<div class="row-fluid">
					
							<div class="span4">		<label><?php echo translate("Email Support"); ?></label></div>
								<div class="span5 cont">	<?php if(isset($row->email) && $row->email != '') echo $row->email; else echo "-" ?></div>
							
						</div>
						<div class="row-fluid">
					
		<div class="span4">		<label><?php echo translate("Meet us at"); ?></label></div>
			<div class="span5 cont">	<?php if(isset($row->name) && $row->name != '') echo $row->name; else echo "-" ?></div>
				
						</div>
				<div class="row-fluid">		
					<div class="span4 contact_hidden">	</div>
					<div class="span5 cont">	
			<?php if(isset($row->street)) echo ''.$row->street.''; ?>
			</div>
			</div>
			<div class="row-fluid">		
					<div class="span4 contact_hidden">	</div>
					<div class="span5 cont">	
		<?php if(isset($row->city)) echo ''.$row->city.''; ?>
	     	</div>
			</div>
			<div class="row-fluid">		
					<div class="span4 contact_hidden">	</div>
					<div class="span5 cont">
			<?php if(isset($row->state)) echo $row->state; ?>&nbsp;-&nbsp;<?php if(isset($row->pincode) && $row->pincode != '0') echo $row->pincode; else "-"; ?>
			</div>
			</div>
				<div class="row-fluid">		
					<div class="span4 contact_hidden">	</div>
						<div class="span5 cont">
     <?php if(isset($row->country)) echo ''.$row->country.''; ?>
         </div>
             </div>
            </div>
           </div>
             <div class="span7">
             	<div class="clsFloatRight" id="Contact_Right">

<!-- Feedback Form start -->


      <form action="<?php echo site_url('pages/contact'); ?>" id="submit_message_form" method="post">                

      <div class="row-fluid">
        <div class="span3"><label class="inner_text span2" for="name"><?php echo translate("Name"); ?><sup>*</sup></label></div>
        	<div class="span5"><input id="name" name="name"class="span12" placeholder="Name" type="text" value="<?php echo set_value('name'); ?>" />
     	 <?php echo form_error('name'); ?></div>
     	 </div>
        <div class="row-fluid">
      <div class="span3">  <label class="inner_text" for="email"><?php echo translate("Email Address"); ?><sup>*</sup></label></div>
     <div class="span5"><input id="email" name="email" class="span12" placeholder="Email Address" type="text" value="<?php echo set_value('email'); ?>" />
       <div  style="margin-top:10px;"><?php echo form_error('email'); ?></div>
     </div></div>
				
				<div class="row-fluid">		
      <div class="span3">  <label class="inner_text" for="message"><?php echo translate("Feedback"); ?><sup>*</sup></label></div>
       <div class="span5"> <textarea id="message" class="span12" name="message" value="Feedback" rows="4"><?php echo set_value('message'); ?></textarea>
       <?php echo form_error('message'); ?></div>
    </div>	
			 		
     <div class="row-fluid">	
     	<div class="span3"></div>	
        <div class="span5"><label>&nbsp;</label>
        <button id="message_submit" name="commit" class="button2" type="submit"><span><span><?php echo translate("Send"); ?></span></span></button>
</div>
</div>
    </form>
                                    


<!-- End of feedback form  -->

</div>
</div>
            <div style="clear:both"></div>
</div>
</div>
</div>
</div>

</div>
  <!-- END STATIC LAYOUT -->
</div>
</div>