<!-- Required css stylesheets -->
<link href="<?php echo css_url().'/dashboard.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<div class="container-fluid">
<!-- End of stylesheet inclusion -->
<?php $this->load->view(THEME_FOLDER.'/includes/dash_header'); ?>

  <?php $this->load->view(THEME_FOLDER.'/includes/travelling_header'); ?>
<div id="dashboard_container">
	<div class="row-fluid">
		<div class="span12">
  <div class="Box" id="View_Starred_Items">
      <div class="Box_Head msgbg">
        <h2><?php echo translate("Starred Items"); ?></h2>
      </div>
      <div class="Box_Content span12" style="margin-left:0px;">
        <?php if(!empty($starred))
                           {
                                        
						   if($this->dx_auth->is_logged_in())
												{
																$id = $this->dx_auth->get_user_id();
																if($starred=='true')
															 $this->db->where('starred',$starred);
																//$this->db->or_where('user_id',$id);
																$query = $this->db->get_where('list');
																if( $query->num_rows > 0 )
																{
																		 foreach($query->result() as $row)
																			{
																							echo '<li class="listing">
																																<div class="thumbnail">';
																							
																							echo '<img alt="Host_pic" src="http://www.cogzidel.com/images/host_pic.gif" /></div>';
																							echo '<div class="listing-info"><h3>';
																																		echo anchor('rooms/'.$row->id,$row->title);
																																		echo '</h3>';
																																		echo '<span class="actions">
																																				<span class="action_button">';
																																		echo anchor('func/editListing/'.$row->id,"Edit Listing",array('class' => 'icon edit'));
																																		echo '</span>
																																				<span class="action_button">';
																																		echo anchor('rooms/'.$row->id,"View Listing",array('class' => 'icon view'));
																																		echo '</span>
																																		<span class="action_button">';
																																		echo anchor('func/deletelisting/'.$row->id,"Delete Listing",array('class' => 'icon view'));
																																		echo '</span>
																																		</div>
																															<div class="clear"></div>
																														</li>';
										}     		
							} 
							}
							}
							else
							{
		?>
        <div id="searching">
                                            <?php echo form_open("search",array('id' => 'search_form')); ?>  
                                            <p><?php echo translate("You have no starred Items."); ?> </p>
                                            <div class="row-fluid">
                                            
                                            <div class="span4">
                                            <p><input value="<?php echo translate("Where are you going?"); ?>" onclick="clear_location(this);" type="text" class="span5" autocomplete="off" id="location" name="location" />
                                            </div>
                                         </div>
                                         <button id="submit_location" class="gotomsg" onclick="if (check_inputs()) {$('#search_form').submit();}return false;" type="button" name="submit_location"><span><span><?php echo translate("Search"); ?></span></span></button></p>
										 <?php echo form_close(); ?>
                            </div>
        <?php echo form_close(); ?>
        <?php } ?>
      </div>
  </div>
</div>
</div>
</div>

</div>

<!-- Footer Scripts -->
<script type="text/javascript">
    function is_not_set_location() { return (!$('#location').val() || ("Where are you going?"== $('#location').val())) }
    
    function clear_location (box) {
        if (is_not_set_location()) box.value = '';
    }
    
    function check_inputs() {
        if (is_not_set_location()) { alert("Please set location"); return false; }
        return true;
    }
</script>

