<script src="<?php echo base_url(); ?>js/swfobject.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/jwplayer.js" type="text/javascript"></script>
<style>
	.gotomsg
	{
    background-color: rgb(192, 192, 192);
    box-shadow: 0 11px 8px -10px rgb(0, 0, 0);
    color: rgb(255, 255, 255);
    font-weight: bold;
    font-size:17px;
   padding:13px 20px 10px 20px;
   border-radius:5px;
    text-transform: uppercase;
    }
    .gotomsg:hover
    {
    	text-decoration:none;
    	color:white;
    }
.Howit_Img img{
    	height:130px;
    }
   .span4
    {
    	border:1px solid #c0c0c0;
    	padding:10px 10px 20px 10px;
    	border-radius:5px;
    	margin-bottom:10px;
    }
</style>
<div class="container" style="margin-top:4%;">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2"></div>
			
					<div class="container_bg1 container_bg_non" id="View_HowIt" style="padding:0!important;">
<div class="How_It_VideoBg">
	<?php if($display_type == 0) { ?>
    <div id="mediaplayer"><?php echo translate("JW Player goes here") ; ?></div>
    <?php } else { 
    echo $embed_code;
    } ?>
 </div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div class="span4">
<div id="How_It_Blk" class="clearfix">
	<div class="How_It1 clsFloatLeft">
    	<h2><?php echo translate("Find a place"); ?></h2>
        <div class="Howit_Img"><img src="<?php echo css_url(); ?>/images/find_places.png" /></div>
        <p><a href="<?php echo site_url('search'); ?>" class="gotomsg span12"><?php echo translate("Search"); ?></a></p>
    </div>
   </div>
  </div>
   <div class="span4">
    <div class="How_It2 clsFloatLeft">
    	<h2><?php echo translate("Add your Place"); ?></h2>
        <div class="Howit_Img"><img src="<?php echo css_url(); ?>/images/add_place.png" /></div>
        <p><a href="<?php echo site_url('rooms/new'); ?>" class="gotomsg span12"><?php echo translate("List Your Space"); ?></a></p>
    </div>
   </div>
    <div class="span4">
    <div class="How_It3 clsFloatLeft">
    	<h2><?php echo translate("Why Host"); ?></h2>
        <div class="Howit_Img"><img src="<?php echo css_url(); ?>/images/why_hosts.png" /></div>
      <p><a href="<?php echo site_url('pages/view').'/why_host'; ?>" class="gotomsg span12"><?php echo translate("Learn More_how"); ?></a></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
jwplayer("mediaplayer").setup({
flashplayer: "<?php echo base_url(); ?>uploads/howit/player.swf",
file: "<?php echo base_url(); ?>uploads/howit/<?php echo $media; ?>",
height:429


});
</script>
