		<p style="padding:10px 0;"><?php echo translate("We can send money to users in"); ?> <b><?php echo $country; ?></b> <?php echo translate("as follows:"); ?></p>
		<div id="payout_method_descriptions" class="clsTable_View">
					
										<div class="row-fluid">
											<div class="span12 Acct_back">
													<div class="span2"><?php echo translate("Method"); ?></div>
														<div class="span2"><?php echo translate("Arrives On*"); ?></div>
														<div class="span2"><?php echo translate("Fees"); ?></div>
														<div class="span6"><?php echo translate("Notes"); ?></div>
											</div>
										</div>
												
											<?php foreach($result->result() as $row) { ?>
											<div class="row-fluid">
											<div class="span12">
														<div class="span2 type pay_ver"><?php echo $row->payment_name; ?></div>
														<div class="span2 pay_ver"><?php echo $row->arrives_on; ?></div>
														<div class="span2 pay_ver"><?php echo $row->fees; ?></div>
														<div class="span6"><?php echo $country; ?><br><?php echo $row->note; ?></div>
											</div>
										</div>
												
											<?php } ?>
											
						</tbody>
		</div>
		<div style="font-size:15px; margin:5px 0px 10px 0px;">* <?php echo translate("Money is always released the day after check in but may take longer to arrive to you."); ?></div>
		<form  method="post" action="<?php echo base_url().'account/paymentInfo'; ?>">
		<p><input type="hidden" value="<?php echo $country_symbol;?>" name="country">
		
		<?php echo translate("What method would you prefer?"); ?> 
		<select name="payout_type" id="payout_info_type">
		<?php foreach($result->result() as $row) {  ?>
				<option value="<?php echo $row->id; ?>"><?php echo $row->payment_name; ?></option>
	<?php } ?>
		</select>
   &nbsp;&nbsp;<button type="button" class="gotomsg" name="commit" id="next2"><span><span><?php echo translate("Next"); ?></span></span></button>
			 <?php echo translate("or"); ?>
			&nbsp;<a onclick="$('#payout_new_select').hide();$('#payout_new_initial').show();return false;" href="#"><?php echo translate("Cancel"); ?></a></p>
		</form>
		
<script type="text/javascript">

$(document).ready(function (){

$("#next2").click(function(){ 

var payout_type = $("#payout_info_type").val();
var country     = $("#country").val();

	$.ajax({
	 type: "POST",
		url: "<?php echo site_url('account/paymentInfo'); ?>",
		async: true,
		data: "payout_type="+payout_type+"&country="+country,
		success: function(data)
			{	
					$("#payout_new_select").html(data);
     $('#payout_country_select').hide();
					$('#payout_new_select').show();
			}
  });

})

});
</script>