<!-- Required css stylesheets -->
<link href="<?php echo css_url().'/dashboard.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<div class="container-fluid">

<!-- End of stylesheet inclusion -->
  <?php $this->load->view(THEME_FOLDER.'/includes/dash_header'); ?>

			<?php $this->load->view(THEME_FOLDER.'/includes/account_header'); ?>	
<div id="dashboard_container">
    <div class="Box" id="View_Transaction">
    	<div class="row-fluid">
    		<div class="span12">
    	<div class="Box_Head msgbg"><h2><?php echo translate("Transaction History"); ?></h2></div>
    	<div class="Box_Content">
			<?php if($result->num_rows() > 0) { ?>
            <div class="clsTable_View">
           
            <div class="row-fluid">
    		<div class="span12 trans_hd">
           <div class="span1"><?php echo translate("List ID"); ?> </div>
            <div class="span2"><?php echo translate("Traveller Name"); ?> </div>
            <div class="span2"><?php echo translate("Host Name"); ?> </div>
            <div class="span2"><?php echo translate("Check In"); ?> </div>
            <div class="span2"><?php echo translate("Check Out"); ?> </div>
           <div class="span1"><?php echo translate("Price"); ?> </div>
			<div class="span2"><?php echo translate("Status"); ?> </div>
           </div>
           </div>
            
           
            
            <?php
												foreach($result->result() as $row) {
												
												$query           = $this->Users_model->get_user_by_id($row->userby);
												$booker_name     = $query->row()->username;
												
												$query1          = $this->Users_model->get_user_by_id($row->userto);
												$hotelier_name   = $query1->row()->username;
												?>
           <div class="row-fluid">
    		<div class="span12 trans_detail">
            <div class="span1"><?php echo $row->list_id; ?> </div>
            <div class="span2"><?php echo $booker_name; ?> </div>
            <div class="span2"><?php echo $hotelier_name; ?> </div>
            <div class="span2"><?php echo get_user_times($row->checkin, get_user_timezone()); ?> </div>
            <div class="span2"><?php echo get_user_times($row->checkout, get_user_timezone()); ?> </div>
            <div class="span1"><?php echo get_currency_symbol($row->list_id).get_currency_value1($row->list_id,$row->price); ?> </div>
												<div class="span2"><?php echo $row->name; ?></div>
            </div>
            </div>
            <?php } ?>
            
            
            </div>
            <?php echo $pagination; } else { ?>
         <p><?php echo translate("Once you have reservations, the money that you have earned will be displayed here."); ?></p>
            <?php } ?>
        </div>
  	</div>
</div>
</div>
</div>
</div>
