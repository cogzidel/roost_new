<link href="<?php echo css_url().'/dashboard.css'; ?>" media="screen" rel="stylesheet" type="text/css" />
<style>
#left {
/*width: 259px;
float:left;*/

}
#main {
   /* float: left;*/
  
    /*width: 692px;*/
   
}
.thumbnail img
{
	padding:0px !important;
	margin:0px !important;
}
#main p {
padding:0 0 10px 0;
}
.clsH1_long_Border h1 {
background:#c0c0c0;
font-size:18px;
padding:12px 10px 9px;
margin:0 0 10px 0;
position:relative;
overflow:hidden;
color:white;
text-transform: uppercase;
font-weight: normal;

border-radius:8px;

border-radius: 8px;
}
.vouch_addr
{
	font-size:17px;
}
.clsH1_long_Border h1 span {
position:absolute;
right:10px;
top:5px;
}
</style>
<!-- End of style sheet inclusion -->
<div class="container-fluid">
<div class="container_bg" id="View_Vouch">
	<div class="row-fluid">
		<div class="span12">

  <div id="dashboard" class="clsDes_Top_Spac">
  	
  			 	
    <div>
      <div class="clsH1_long_Border span12">
      	<?php //print_r($user->username);exit;?>
        <h1 style="line-height:normal;"> <?php echo ucfirst($user->username); ?>
								<?php if( ($this->dx_auth->is_logged_in()) || ($this->facebook_lib->logged_in())){ ?>
								<p style="float:right;"><?php echo translate("Member from"); ?> <?php echo get_user_times($user->created, get_user_timezone()); ?></p> 
								<?php } ?>
							 </h1>
      </div>
      <div style="clear:both"></div>
    </div>
    
    <div style="clear:both"></div>
   <div class="row-fluid">
  		<div class="span12">
  			 <div class="span3">
  			 	
    <div id="left">
      <div id="user_box" class="Box">
          <div class="Box_Content">
            <div id="user_pic" onClick="show_ajax_image_box();" style="text-align:center; padding:0 0 10px 0;"> 
												
												<img width="100%" src="<?php echo $this->Gallery->profilepic($user->id, 2); ?>" />
    </div>
          </div>
          <!-- middle -->
      </div>
      <!-- /user -->
    </div>
    </div>
    
    <!-- /left -->
    <div class="span9">
    	
    <div id="main">
					 <?php //if( ($this->dx_auth->is_logged_in()) || ($this->facebook_lib->logged_in()) || ($this->twitter->is_logged_in()) ){
					 	if( ($this->dx_auth->is_logged_in()) || ($this->facebook_lib->logged_in())){ ?> 
					 	
      <div class="Box">
      <div class="Box_Head msgbg">
              <h2><?php echo translate("Vouch for"); ?> <?php echo ucfirst($user->username);?></h2>
            </div>
          <div class="Box_Content">
            <?php echo form_open('users/vouch/'.$this->uri->segment(3)); ?>
            <p style="font-weight:normal; font-style:italic; font-size:16px;"> <?php echo translate("Please write a few sentences explaining why"); ?> &nbsp;<?php echo ucfirst($user->username);?> &nbsp;<?php echo translate("is a great person."); ?> </p>
            <p><?php echo translate("Enter your recommendation here and then click the Recommend button."); ?> </p>
            <input type="hidden" name="userto" value="<?php echo $this->uri->segment(3); ?>">
            <input type="hidden" name="userby" value="<?php echo $this->dx_auth->get_user_id(); ?>">     
           	           	    
           	           	    
           	           	     <p>
              <textarea id="recommend" class="span8" name="message" cols="75"> </textarea>
            </p>
           
            				<span style="color:#FF0000"><?php echo form_error('message'); ?></span>
            <p>
              <button name="friends_recommend" class="gotomsg" type="submit"><span><span><?php echo translate("Recommend"); ?></span></span></button>
            </p>
            <?php echo form_close();?> 
												</div>
      </div>
						<?php } ?>
      <!--List-->
      <div class="Box"s>
      <div class="Box_Head msgbg">
          <h2><?php echo translate("My Listing"); ?></h2>
          </div>
          	
        <div class="Box_Content" style="background-color:white">
          
          <table id="user_result_list" class="span12" width="100%">
          
            <tbody class="span12">
   <?php
	  if($lists->num_rows() > 0)
	  {
		 foreach($lists->result() as $list)
			{
				?>
								<tr class="even span12" id="room_<?php echo $list->id; ?>">
									
									<td class="place_image span1"><a class="thumbnail" href="<?php echo base_url().'rooms/'. $list->id; ?>"><img width="75" height="50" title="Test room" src="<?php echo getListImage($list->id); ?>" alt="Test room"><span><img width="100" height="100" title="Test room" src="<?php echo getListImage($list->id); ?>" alt="Test room"></span></a> </td>
									
										<td class="main span11"><div class="first-line title"><a href="<?php echo base_url().'rooms/'. $list->id; ?>"><?php echo $list->title;?></a></div>
												<div class="vouch_addr"><?php echo $list->address; ?></div></td>
									</tr>
									
     
									<?php }	} else { echo translate("There is no List"); } ?>
            </tbody>
          </table>
         
           </div>
        </div>
     
     
      <!--List-->
      <!--Recommendation-->
        <div class="Box">
        <div class="Box_Head msgbg">
        	<h2><?php echo translate("Recommendations"); ?></h2>
        </div>
								
        <div class="Box_Content">
            
			<div style="width:100%;" class="quotes span12" id="vouch_recom_tab">
					
					<?php 
					if($recommends->num_rows() > 0)
					{
						foreach($recommends->result() as $row)
						{
							if($this->db->where('id',$row->userby)->get('users')->num_rows()!=0)
							{
					?>
						<div class="row-fluid">
							<div class="span12">
								<div class="span2">
                                <div class="review_prof_img">
                                <a onclick="window.open(this.href);return false;" href="<?php base_url();?>"><img width="100%" height="76" title="Mahes W" src="<?php echo $this->Gallery->profilepic($row->userby, 1);  ?>" alt="Mahes W"></a><a target="blank" href="<?php echo site_url('users/profile').'/'.$row->userby; ?>"><?php echo get_user_by_id($row->userby)->username; ?></a>
                                </div>
										</div>
								<div class="sapn10" style="margin-right:10px;vertical-align: top">
                                <div class="review_right_content span10">
														<?php echo $row->message;?>
                                                        <span class="review_right_arrow"></span>
										</div>
                                      </div>
                                       </div>
                                      </div>
						 <?php }
						} } else {  echo '<p>'.translate("There is no Recommend").'</p>'; } ?>
             
           </div>
          </div>
        </div>
      </div>
      </div>
      
      <!--Recommendation-->
    <!-- /main -->
    <div class="clear"></div>
    </div>
    </div>
  </div>
  </div>
  </div>
  <!-- /dashboard -->
</div>
</div>
<!-- /command_center -->