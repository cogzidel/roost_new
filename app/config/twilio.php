<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Twilio
*
* Author: Ben Edmunds
* ben.edmunds@gmail.com
* @benedmunds
*
* Location:
*
* Created: 03.29.2011
*
* Description: Twilio configuration settings.
*
*
*/

/**
* Mode ("sandbox" or "prod")
**/
$config['mode'] = 'sandbox';

/**
* Account SID
**/
$config['account_sid'] = 'ACa8f37807954a08b6f992ebc507c83f46';

/**
* Auth Token
**/
$config['auth_token'] = '5282faad3718d740d1886a4967fb2c23';

/**
* API Version
**/
$config['api_version'] = '2010-04-01';

/**
* Twilio Phone Number
**/
$config['number'] = '+192592209911';
?>